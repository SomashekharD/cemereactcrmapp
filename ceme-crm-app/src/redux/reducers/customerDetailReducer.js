import custDetailTypes from "../../redux/actions/actionTypes";

export const CRM_REDUCER_INITIAL_STATE = {
	custDetails: [],
	loading: false,
	error: false,
};

const customerDetailReducer = (state = CRM_REDUCER_INITIAL_STATE, action) => {
	switch (action.type) {
		case custDetailTypes.GET_CUSTOMER_DETAIL_BEGIN:
			return {
				...state,
				loading: true,
			};
		case custDetailTypes.GET_CUSTOMER_DETAIL_SUCCESS:
			return {
				
				...state,
				custDetails: action.payload,
				loading: false,
			};
		case custDetailTypes.GET_CUSTOMER_DETAIL_FAILURE:
			return {
				...state,
				error: action.payload,
			};
		default: {
			return state;
		}
	}
};

export default customerDetailReducer;
