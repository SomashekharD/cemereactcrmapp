import React, {useEffect, useState} from  'react';
import { useDispatch, useSelector } from "react-redux";
import { getCustomerInteractionDetail } from "../../redux/actions/getCustInteractions";
import {  useParams, Link} from 'react-router-dom';

const CustInteraction = () =>  {
    
    const { custid } = useParams();
    const [id,setId] = useState(custid);
    const dispatch = useDispatch();
    const { custInteractions, loadingInt,errorInt } = useSelector((state) => state.interactionDetail);
    const  formatDate =(date) => {
        let datetime = date.getDate() + "/"+ parseInt(date.getMonth()+1)  +"/"+ date.getFullYear() + " " +
        date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

       return datetime;
    }

    useEffect(() => {
        if(id)
        {
            dispatch(getCustomerInteractionDetail(id));
        }
        else
        {
            setId(custid);
            //history.push("/viewCustomer");
        }
       
    }, [custid,id,dispatch]);

    const expandCollapseDet = (e) => {
        e.target.classList.toggle("active");
       if(e.target.parentElement)
       {
           if(e.target.parentElement.nextElementSibling.classList.value.includes("paneldet"))
           {
            e.target.parentElement.nextElementSibling.classList.value = "panel";
           }
           else{
            e.target.parentElement.nextElementSibling.classList.value = "paneldet";
           }
       }
    }

    if (errorInt) {
        
		return (
            <div className="alert alert-danger" role="alert">
                {errorInt.message}
                </div>
        );
	}

    if (loadingInt) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
    }

    if(custInteractions.length === 0)
    {
        return (
            <div className="col-md-12 inner">
            <div className="col-md-12 tabsContainer"></div>
            <div className="col-md-12 alert alert-info"  role="alert">
                 No Interactions Info found.
            </div>
            </div>
		);
    }

    return(
        <div className="col-md-12 inner">
        <div className="col-md-12 tabsContainer">
       <br></br>
        <h4  className="my-0 font-weight-primary text-center" style={{color: "#007bff"}}>   Interaction Details </h4>
        <br></br>
       
        <div>

        <div className="col mod-12 text-right">
       
       <button type="button" className="btn btn-primary"><Link to = {"/viewCustomer"}>Back to Details</Link></button>
      <br></br>
       </div>
        
            </div>
            <br></br>
       { custInteractions.map((interaction) => {
            return(
                <div key={interaction.id}>
                    
                    <div className="col-md-12 panelContainer" onClick={expandCollapseDet} style={{ display:"inline-flex",paddingLeft:"0px"}}>
                        <button id="p1" className="accordion" > Interaction On  -  {formatDate(new Date(interaction.createdDate)) } </button>
                    </div>   
                    <div className="col-md-12 panel" style={{paddingLeft:"0px"}} id="policydet">
                    <div className="detailbackgroudcolor">
                    <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
                            <div className="col-md-3 detailtextfont">
                                Notes:
                                </div>
                                <div className="col-md-3">
                                { interaction.notes}
                                </div>
                                <div className="col-md-3 detailtextfont">
                                Policy Number:
                                </div>
                                <div className="col-md-3">
                                {interaction.policyNumber}
                                </div>
                        </div>
                        <div className="col-md-12 my-1 text-left" style={{ display:"inline-flex"}}>
                            <div className="col-md-3 detailtextfont">
                                Phone:
                                </div>
                                <div className="col-md-3">
                                {interaction.phone}
                                </div>
                                <div className="col-md-3 detailtextfont">
                                Support Person:
                                </div>
                                <div className="col-md-3">
                                {interaction.createdBy}
                                </div>
                        </div>
                    </div>   
                    </div>
               
        </div>
                )}
                )
       }

</div>
</div>
)
    };

export default CustInteraction;