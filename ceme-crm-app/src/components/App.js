import NavBar from "./common/NavBar";
import HomePage from "./HomePage";
import AboutUS from "./AboutUs";
import AddCustomerPage from "./AddCustomerPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import Footer from "./common/Footer";
import ManageDetailView from './CustomerDetailView/ManageDetailView';
import CustInteraction from './CustomerDetailView/CustInteraction';
import GreetingCustomer from "./common/GreetingCustomer";
import PageNotFound from "./common/PageNotFound";

function App() {
	const customer = useSelector((state) => state.search.customer);
	return (
		<Router>
			<NavBar />
			<GreetingCustomer customer={customer} />
			<main role="main" className="flex-shrink-0">
				<div className="container">
					<Switch>
						<Route exact path="/">
							<HomePage />
						</Route>
						<Route path="/addcustomer">
							<AddCustomerPage />
						</Route>
						<Route path="/aboutus">
							<AboutUS />
						</Route>
						<Route path="/viewCustomer">
							<ManageDetailView />
						</Route>
						<Route path="/viewInteraction/:custid">
							<CustInteraction />
							</Route>
						<Route>
							<PageNotFound />
						</Route>
					</Switch>
				</div>
			</main>
			<Footer />
		</Router>
	);
}

export default App;
